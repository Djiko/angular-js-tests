'use strict';

var myApp = angular.module('angularTestApp', []);

myApp.controller('UserCtrl', ['$scope', function ($scope) {
	$scope.user = {};
	$scope.user.details = {
      'username': 'Todd Motto',
      'id': '89101112'
    };
}]);
