'use strict';

var myApp = angular.module('angularTestApp', []);
myApp.config(function($httpProvider) {
	$httpProvider.defaults.useXDomain = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

myApp.controller('MainCtrl', function ($scope) {
	$scope.text = 'Hello, World.';
	$scope.greeting = 'Gilles Coulais';
});

myApp.controller('UserCtrl', ['$scope', function ($scope) {
	$scope.user = {};
	$scope.user.details = {
      'username': 'Gilles Coulais',
      'id': '1'
    };
}]);

myApp.controller('FreeboxController', function ($scope, freeboxService) {
	$scope.data = null;
	freeboxService.getData(function(dataResponse) {
		console.log(dataResponse);
		$scope.data = dataResponse;
	});
});

myApp.service('freeboxService', function($http) {
	this.getData = function(callbackFunc) {
		$http({
			method: 'GET',
			url: 'http://mafreebox.freebox.fr/api_version',
		}).then(function(response) {
			console.log('Results -> ' + response.data);
			callbackFunc(response.data);
		});
	};
});

myApp.directive('customButton', function () {
	return {
		restrict: 'A',
		replace: true,
		transclude: true,
		templateUrl:'templates/customButton.html',
		/*
		link: function (scope, element, attrs) {
			// DOM manipulation/events here!
		}
		*/
	};
});

myApp.filter('reverse', function () {
	return function (input, uppercase) {
		var out = '';
		for (var i = 0; i < input.length; i++) {
			out = input.charAt(i) + out;
		}
		return uppercase ? out.toUpperCase() : out;
	};
});
